INFORMATION FOR DEVELOPERS

This module creates a very simple drupal/server alive ajax ping back for HTML5
monitoring of multiple sites in agencies.

the json is structured as follows

{ 
	lifeforce: BOOL 
	timestamp : unixtime
}

============================================================================
Preconfigured menu paths
============================================================================

/halloumi

this path is not protected via user level access as the information shown is
simply to to inform a JS script at the current server time the server exists and
500/404's would be detected in the client's Ajax code.

============================================================================
Suggested Use
============================================================================

Halloumi is a simple module that serves a very particular need which is
monitoring sites at the core level with minimum overhead on a mobile device.

As chrome / iOS allow packaged HTML5 apps, a simple UI can be made to show the
required drupal servers you run and inform a sys admin quickly and effectively
on any HTML device if that server is active and that drupal exists. Which is a
significantly lacking facility within Drupal 7's core for agencies.

Combined with local storage and html offline storage, this can serve as a in
agency display board method too. With the minium of resources needed and the
minimum of overhead on all your drupal sites.
